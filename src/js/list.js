function List (data) {
   this.data = data;
}
List.prototype.create = function () {
    var list = d3.select('.list-container').append('svg')
        .attr('class', 'auto-list-text')
        .attr('width', 60);

    list.selectAll('text')
        .data(this.data)
        .enter()
        .append('text')
        .attr('x', 20)
        .attr('width', 60)
        .attr('height', 20)
        .sort(function (a,b){ return d3.descending(a.currentValue, b.currentValue)})
        .text(function (d) {
            return d.label;
        }).each(function (d, i) {
            d3.select(this)
                .style('fill', d.color);
        });

    list = d3.select('.list-container').append('svg')
        .attr('class', 'auto-list-values')
        .attr('width', 60);

    list.selectAll('text')
        .data(this.data)
        .enter()
        .append('text')
        .attr('x', 20)
        .attr('width', 60)
        .attr('height', 20)
        .sort(function (a,b){ return d3.descending(a.currentValue, b.currentValue)})
        .text(function (d) {
            return d.currentValue;
        }).each(function (d, i) {
            d3.select(this)
                .style('fill', d.color);
        });

    return this;
};

List.prototype.update = function () {
    var list = d3.select('svg.auto-list-text');

    console.log("updated " + this.data[0].currentValue);
    list.selectAll('text')
        .data(this.data, function(d) { return d.label; })
        .sort(function (a,b){ return d3.descending(a.currentValue, b.currentValue)})
        .each(function (d, i) {
            d3.select(this)
                .transition()
                .duration(300)
                .attr('y', 30 + i * 30)
                .text(function (d) {
                    return d.label;
                });
        });

    list = d3.select('svg.auto-list-values');

    console.log("updated " + this.data[0].currentValue);
    list.selectAll('text')
        .data(this.data, function(d) { return d.label; })
        .sort(function (a,b){ return d3.descending(a.currentValue, b.currentValue)})
        .each(function (d, i) {
            d3.select(this)
                .transition()
                .duration(300)
                .delay(100)
                .attr('y', 30 + i * 30)
                .text(function (d) {
                    return d.currentValue;
                });
        });

    return this;
};
