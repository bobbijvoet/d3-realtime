var RealTimeData = function (layers) {
    this.stocks = layers;
    this.timestamp = ((new Date()).getTime() / 1000) | 0;
    this.demping = 10;
    this.crisis = false;
};

RealTimeData.prototype.rand = function (value) {
    var plusminus = Math.floor(Math.random() * 2) + 1;


    if (this.crisis) {
        value +=  (plusminus === 2 ? 100 : -200)/this.demping;
    } else {
        console.log('plus ' + plusminus);
        value +=  (plusminus === 2 ? 200: -100)/this.demping;
    }

    if (value < 0)  {
        value = 0;
    }

    return value;
};

RealTimeData.prototype.history = function (entries) {
    if (typeof(entries) != 'number' || !entries) {
        entries = 1;
    }

    var history = [];


    for (var k = 0; k < this.stocks.length; k++) {
        history.push({values: []});
    }


    for (var i = 0; i < entries; i++) {
        for (var j = 0; j < this.stocks.length; j++) {
            this.stocks[j].currentValue = this.rand(5);
            history[j].values.push(this.stocks[j].currentValue);
        }
        this.timestamp++;
    }

    return history;
};

RealTimeData.prototype.next = function () {
    var entry = [];
    for (var i = 0; i < this.stocks.length; i++) {
        console.log("BEFORE " + this.stocks[i].currentValue );
        this.stocks[i].currentValue = this.rand(this.stocks[i].currentValue | 0);
        entry.push({
            time: this.timestamp,
            label: this.stocks[i].label,
            color: this.stocks[i].color,
            y: this.stocks[i].visible ? this.stocks[i].currentValue : null
        });
    }
    this.timestamp++;
    return entry;
};

window.RealTimeData = RealTimeData;
