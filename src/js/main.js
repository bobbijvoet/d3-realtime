$(function () {


    var stockData = [
        {
            label: 'ing',
            color: 'orange',
            visible: true
        },
        {
            label: 'shell',
            color: 'green',
            visible: true
        },
        {
            label: 'kpn',
            color: 'blue',
            visible: true
        },
        {
            label: 'OCI',
            color: 'pink',
            visible: true
        }
    ];
    var data = new RealTimeData(stockData);
    var chart = $('#chart')
        .epoch({
            type: 'time.line',
            data: data.history(10),
            axes: ['left', 'bottom', 'right']
        });


    var container = d3.select('#chart');
    var legend = container.append("svg")
        .attr('class', 'legend')
        .attr('width', 100)
        .attr('height', 200).append('g')
        //.attr("transform", 'translate(200,0)')
        .attr("y", 225)
        .attr("height", 100)
        .attr("width", 100);

    legend.selectAll('g').data(stockData)
        .enter()
        .append('g')
        .each(function (d, i) {
            var g = d3.select(this);
            g.attr('data-visible', true);
            g.attr('class', 'pointer');
            g.append("rect")
                .attr("x", 0)
                .attr("y", 8 + i * 25)
                .attr("width", 10)
                .attr("height", 10)
                .style("fill", d.color);

            g.append("text")
                .attr("x", 5 + 10)
                .attr("y", 18 + i * 25)
                .attr("height", 30)
                .attr("width", 100)
                .style("fill", d.color)
                .text(d.label);
            g.on('click', function () {
                g.select('text').style('opacity', '0.2');
                g.select('rect').style('opacity', '0.2');
                if (g.attr('data-visible') === 'true') {
                    stockData[i].visible = false;
                    g.select('text').style('opacity', '0.2');
                    g.select('rect').style('opacity', '0.2');
                } else {
                    stockData[i].visible = true;
                    g.select('text').style('opacity', '1');
                    g.select('rect').style('opacity', '1');
                }
                g.attr('data-visible', g.attr('data-visible') !== 'true');

            });

        });


    window.setInterval(function () {
        var x = data.next();
        chart.push(x);
        list.update();
    }, 1000);
    chart.push(data.next());

    var list = new List(data.stocks).create();

    $('[demping]').on('keyup', function () {
        data.demping = parseInt($(this).val());
    });

    $('[crisis]').change(function () {
        data.crisis = $(this).is(':checked');
    })
});
